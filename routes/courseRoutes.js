const express = require("express");
const courseController = require("../controllers/courseControllers");
const router = express.Router();

router.get("/", (req, res) => {
    courseController.getAllCourses().then((result) => {
        res.send(result);
    });
});

router.post("/create-course", (req, res) => {
    courseController.createCourse(req.body).then((result) => {
        res.send(result);
    });
});

router.get("/active-courses", (req, res) => {
    courseController.getActiveCourses().then((result) => {
        res.send(result);
    });
});

router.get("/get-course", (req, res) => {
    courseController.getSpecificCourse(req.body.courseName).then((result) => {
        res.send(result);
    });
});

router.get("/:id", (req, res) => {
    courseController.getById(req.params.id).then((result) => {
        res.send(result);
    });
});

router.put("/archive-course", (req, res) => {
    courseController.archieveCourse(req.body.courseName).then((result) => {
        res.send(result);
    });
});

router.put("/unarchive-course", (req, res) => {
    courseController.unarchieveCourse(req.body.courseName).then((result) => {
        res.send(result);
    });
});

router.put("/archive/:id", (req, res) => {
    courseController.archieveCourseById(req.params.id).then((result) => {
        res.send(result);
    });
});

router.put("/unarchive/:id", (req, res) => {
    courseController.unArchieveCourseById(req.params.id).then((result) => {
        res.send(result);
    });
});

router.delete("/delete-course", (req, res) => {
    courseController.deleteCourse(req.body.courseName).then((result) => {
        res.send(result);
    });
});

router.delete("/delete/:id", (req, res) => {
    courseController.deleteCourseById(req.params.id).then((result) => {
        res.send(result);
    });
});

module.exports = router;
